
// ==================================================
// Authors: 
// 
// Viktor codeberg.org/viktor
// Kate codeberg.org/kate
// ==================================================

// ==================================================
// Strings for output objects
// ==================================================
const outputStringsCookie = {
  SecureSessionCookies: `Output string for secure session cookies`,
  HttpOnlyForCookies: `Header always edit Set-Cookie (.*) "$1; HttpOnly"`,
  CookieSameSite: `Header always edit Set-Cookie (.*) "$1; Secure"`,
  AlwaysSameSiteStrict: `Header always edit Set-Cookie (.*) "$1; SameSite=strict"`,
};

const outputStringsServerInsights = {
  removeServerInfo: `Output string for remove server information`,
  removeInfoXpoweredBy: `Header always unset X-Powered-By`,
  xModPagespeed: `Header unset X-Mod-Pagespeed`,
  removePingbackInfo: `Header unset X-Pingback`,
  removeCloudflareInfo: `Output string for remove cloudflare info`,
  removeXframeBroswerViewDeny: `Header set X-Frame-Options "deny"`,
  removeXframeBroswerViewSameOrigin: `Header always set Referrer-Policy "same-origin"`,
  removeXframeBroswerViewAllowFrom: `Output string for remove X-Frame Browser View "usecase3 - allow-from https://example.com"`,
};

// ==================================================
// boolean variables selected from the DOM (cookie)
// ==================================================
const secureSessionCookiesDOM = document.getElementById(
  "secure-session-cookies"
);
const httpOnlyForCookiesDOM = document.getElementById("http-only-for-cookies");
const cookieSameSiteSecureDOM = document.getElementById(
  "cookie-same-site-secure"
);
const setAlwaysSameSiteStrictDOM = document.getElementById(
  "set-always-same-site-strict"
);

// ==================================================
// boolean variables selected from the DOM (server insights)
// ==================================================
const serverInfoDOM = document.getElementById("remove-server-info");
const removePoweredByDOM = document.getElementById("remove-powered-by");
const xModPagespeedDOM = document.getElementById("x-mod-pagespeed");
const removePingbackDOM = document.getElementById("remove-pingback");
const removeCloudflareDOM = document.getElementById("remove-cloudflare");
const removeXbrowserViewDOM = document.getElementById("remove-x-browser-view");
const removeXbrowserUsecasesDOM = document.querySelector(
  ".remove-x-frame-usecase"
);

// usecases
const removeXbrowserViewDenyDOM = document.getElementById(
  "remove-x-browser-view-usecase-deny"
);
const removeXbrowserViewSameoriginDOM = document.getElementById(
  "remove-x-browser-view-usecase-sameorigin"
);
const removeXbrowserViewAllowfromDOM = document.getElementById(
  "remove-x-browser-view-usecase-allowfrom"
);

// ==================================================
// submit button DOM selector
// ==================================================
const submitBtn = document.getElementById("submit--btn");

// ==================================================
// function for creating output strings
// ==================================================
const createOutputHTML = function (outputString) {
  const outputDiv = document.createElement("div");
  const outputContent = document.createTextNode(outputString);
  outputDiv.appendChild(outputContent);
  const mainOutputDiv = document.querySelector(".output__text");
  mainOutputDiv.appendChild(outputDiv);
};

// ==================================================
// event listener for clicking "submit"
// ==================================================
submitBtn.addEventListener("click", function () {
  // remove previous content from DOM
  let parentOutput = document.querySelector(".output__text");
  while (parentOutput.firstChild) {
    parentOutput.removeChild(parentOutput.firstChild);
  }

  // logic for which string to show in output
  // ==================================================
  // cookie
  if (secureSessionCookiesDOM.checked) createOutputHTML(outputStringsCookie.SecureSessionCookies);
  if (httpOnlyForCookiesDOM.checked) createOutputHTML(outputStringsCookie.HttpOnlyForCookies);
  if (cookieSameSiteSecureDOM.checked) createOutputHTML(outputStringsCookie.CookieSameSite);
  if (setAlwaysSameSiteStrictDOM.checked) createOutputHTML(outputStringsCookie.AlwaysSameSiteStrict);

  // server insight
  if (serverInfoDOM.checked) createOutputHTML(outputStringsServerInsights.removeServerInfo);
  if (removePoweredByDOM.checked) createOutputHTML(outputStringsServerInsights.removeInfoXpoweredBy);
  if (xModPagespeedDOM.checked) createOutputHTML(outputStringsServerInsights.xModPagespeed);
  if (removePingbackDOM.checked) createOutputHTML(outputStringsServerInsights.removePingbackInfo);
  if (removeCloudflareDOM.checked) createOutputHTML(outputStringsServerInsights.removeCloudflareInfo);

  // usecases
  if (removeXbrowserViewDenyDOM.checked) createOutputHTML(outputStringsServerInsights.removeXframeBroswerViewDeny);
  if (removeXbrowserViewSameoriginDOM.checked) createOutputHTML(outputStringsServerInsights.removeXframeBroswerViewSameOrigin);
  if (removeXbrowserViewAllowfromDOM.checked) createOutputHTML(outputStringsServerInsights.removeXframeBroswerViewAllowFrom);

});

// ==================================================
// event listener for "Remove x-Frame Browser View" checkbox (show/hide usecases)
// ==================================================
removeXbrowserViewDOM.addEventListener("change", function (event) {
  if (event.target.checked) {
    removeXbrowserUsecasesDOM.style = "display: block;";
  } else {
    removeXbrowserUsecasesDOM.style = "display: none;";
    removeXbrowserViewDenyDOM.checked = false;
    removeXbrowserViewSameoriginDOM.checked = false;
    removeXbrowserViewAllowfromDOM.checked = false;
  }
});
